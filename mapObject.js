let mapObject = (object, cb) => {

    for (let key in object) {
        if (typeof (object[key]) !== "function") {
            object[key] = cb(object[key])
        }
    }
    
    return object
}
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }
let cb = (value) => {
    value = value + " " + "added value"
    return value
}

console.log(mapObject(testObject, cb))
